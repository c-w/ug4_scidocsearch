#!/bin/bash

set -e

infile="$1"
inname="$(basename $infile .top)"

scriptdir="$(dirname $(readlink -f $0))"
trec_eval=$scriptdir/bin/trec_eval
truth=$scriptdir/data/truth.rel

precision=$($trec_eval -o -c -M1000 $truth $infile \
            | grep -i -A1 'average precision' \
            | grep -vi 'average precision' \
            | grep -o '[0-9.]*')

rename $inname $inname-$precision $infile

echo $precision
