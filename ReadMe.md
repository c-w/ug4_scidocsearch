# Text Technologies Assessment 1

## Dataset

You are dealing with a collection of scientific documents. Each document
consists of a title, date, authors, abstract, keywords and references, all in
plain-text form. Some elements may be missing in some of the documents.

## Tasks

Your goal is to develop a search engine for retrieving relevant documents in
response to narrative queries. To aid in the development, you are provided with
a set of 32 training queries, a set of relevance judgments, and software for
evaluating the accuracy of your algorithms. Your algorithms will be tested on a
different set of testing queries. Please familiarize yourself with the Data and
Formats section, and then complete the following tasks:

- Implement a simple word overlap retrieval algorithm. Your implementation
  (overlap.py) should read the query file (qrys.txt) and the document file
  (docs.txt) from the current directory. Tokenize on punctuation and lowercase
  the tokens, but don't do anything else to improve performance. For each query
  Q and for each document D compute the overlap score between Q and D. Output
  all scores into a file called overlap.top in the current directory.

- Implement a tf.idf retrieval algorithm (tfidf.py), based on the weighted sum
  formula with tf.idf weighting. Set k=2. All relevant statistics should be
  computed from docs.txt. Tokenize exactly as in task 1. Save the results in a
  file called tfidf.top.

- Try to improve the accuracy of your retrieval algorithms. You are free to use
  any techniques that you think may improve retrieval effectiveness. Use average
  precision to judge the accuracy of your approach. Save your most successful
  algorithm as best.py. Output the results to a file called best.top. Come up
  with a short distinctive name for your algorithm and save it in a file called
  best.id. This name will be used when we report the effectiveness of your
  algorithm. We will run your algorithm on a different set of testing queries.
  Average precision (MAP) on the testing queries will be used as the main
  criterion for marking this task. You can assume that MAP for the training
  queries will be representative of the MAP for the testing queries.

- Evaluate the performance of your algorithms, using the provided trec_eval
  program. You can do this on a Linux command line by running:
  `trec_eval -o -c -M1000 truth.rel {overlap|tfidf|best}.top`
  The program will print a number of standard effectiveness measures for your
  algorithm. Use average precision as your primary measure.

- Write a report outlining the decisions you made in implementing the search
  algorithms, as well as your comments on the relative performance of the
  techniques you have tried. Provide specific details on the methods and their
  parameters. Include formulas, tables and graphs if necessary. You are allowed
  a maximum of 2 pages for the report, but shorter reports are fine.

## Data and Formats

### docs.txt: 3204 documents

Each document is on a separate line. The first field is a unique document
number, followed by the document content, all on the same line. For example:

- Chebyshev Interpolation and Quadrature Formulas of Very High Degree ...
- Generation of Hilbert Derived Test Matrix (Algorithm 274 [F1])...

### qrys.txt: 32 training queries

Each query is given on a separate line. The first field is the query number,
followed by the query itself, as follows:

- 6 SETL, Very High Level Languages
- 28 Anything dealing with star height of regular languages or regular
  expressions or...

### truth.rel: relevance judgments

Specifies which documents are relevant to each query. This file is used by
trec_eval. You do not need to process this file within your code.
