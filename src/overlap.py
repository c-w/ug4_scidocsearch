#!/usr/bin/python


from irsystem import OptionParser, SearchEngine


class WordOverlap(SearchEngine):
    def similarity(self, query, document):
        score = 0

        for query_term in query:
            if query_term in document:
                score += 1

        return score


if __name__ == '__main__':
    parser = OptionParser(calling_module=__file__)
    query_db, document_db, outpath = parser.parse_args()

    search_engine = WordOverlap(query_db, document_db)
    search_engine.write_scores(outpath)
