#!/usr/bin/python


from __future__ import division
import math

from irsystem import OptionParser, SearchEngine


class TfIdf(SearchEngine):
    __repeat_scaling_factor__ = 2
    __idf_cache__ = {}

    def similarity(self, query, document):
        score = 0
        for word in query:
            tf_wq = query[word]
            tf_wd = TfIdf.term_frequency(word, document, self.documents)
            idf_w = TfIdf.inverse_document_frequency(word, self.documents)

            score += tf_wq * tf_wd * idf_w

        return score

    @staticmethod
    def document_frequency(term, collection):
        return sum(document[term] for _, document in collection)

    @staticmethod
    def inverse_document_frequency(term, collection, apply_smoothing=True):
        idf = TfIdf.__idf_cache__.get(term)
        if idf is None:
            df = TfIdf.document_frequency(term, collection)
            C = len(collection)

            if apply_smoothing:
                idf = math.log((C + 1.0) / (df + 0.5))
            elif df == 0:
                idf = 1
            else:
                idf = math.log(C / df)

            TfIdf.__idf_cache__[term] = idf

        return idf

    @staticmethod
    def term_frequency(term, document, collection):
        k = TfIdf.__repeat_scaling_factor__
        d_ratio = len(document) / collection.avg_len
        tf = document[term]

        return tf / (tf + k * d_ratio)


if __name__ == '__main__':
    parser = OptionParser(calling_module=__file__)
    query_db, document_db, outpath = parser.parse_args()

    search_engine = TfIdf(query_db, document_db)
    search_engine.write_scores(outpath)
