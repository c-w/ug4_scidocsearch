#!/usr/bin/python


from __future__ import division
import itertools
import math
import nltk.corpus
import nltk.stem
import string
import warnings

from irsystem import OptionParser, SearchEngine, Tokenizer, Vector
from tfidf import TfIdf


warnings.simplefilter('ignore', DeprecationWarning)  # for old version of NLTK


class Stopwords(SearchEngine):
    __excludes__ = set(nltk.corpus.stopwords.words('english'))
    __length_threshold__ = 2

    @staticmethod
    def exclude(word, no_small=False, no_numbers=False, no_digits=False):
        if word in Stopwords.__excludes__:
            return True

        if no_small and len(word) <= Stopwords.__length_threshold__:
            return True

        if no_numbers:
            try:
                float(word)
                return True
            except ValueError:
                pass

        if no_digits and any(c.isdigit() for c in word):
            return True

        return False

    def process(self, words):
        return [w for w in words if not Stopwords.exclude(w)]


class Ngrams(SearchEngine):
    __ngram_len__ = 4

    @staticmethod
    def ngrams(word):
        if len(word) <= Ngrams.__ngram_len__:
            yield word
        else:
            for n in xrange(len(word) - Ngrams.__ngram_len__ + 1):
                yield word[n:n + Ngrams.__ngram_len__]

    def process(self, words):
        return [ngram for w in words for ngram in Ngrams.ngrams(w)]


class Stemmer(SearchEngine):
    __stemmer__ = nltk.stem.porter.PorterStemmer()

    @staticmethod
    def stem(word):
        return Stemmer.__stemmer__.stem(word)

    def process(self, words):
        return [Stemmer.stem(w) for w in words]


class Lemmatizer(SearchEngine):
    __lemmatizer__ = nltk.stem.wordnet.WordNetLemmatizer()

    @staticmethod
    def lemma(word):
        return Lemmatizer.__lemmatizer__.lemmatize(word)

    def process(self, words):
        return [Lemmatizer.lemma(w) for w in words]


class Wordnet(SearchEngine):
    __separator__ = '_'

    @staticmethod
    def disambiguate(term, context, wider_context=None):
        senses = nltk.corpus.wordnet.synsets(term)
        if not senses:
            return None

        context = Vector(Tokenizer.simple_split(context))
        best_sense, best_score = None, 0
        for sense in senses:
            definition = Vector(Tokenizer.simple_split(sense.definition))
            score = context.dot_product(definition)
            if score > best_score:
                best_sense, best_score = sense, score

        if best_sense is None:
            if wider_context is None:
                best_sense = senses[0]
            else:
                best_sense = Wordnet.disambiguate(term, wider_context)

        return best_sense

    @staticmethod
    def synonyms(term, context, wider_context=None):
        word_sense = Wordnet.disambiguate(term, context, wider_context)
        if word_sense is None:
            return []

        synonyms = []
        for synonym in word_sense.lemma_names:
            if synonym.find(Wordnet.__separator__) > -1:
                for word_compound in synonym.split(Wordnet.__separator__):
                    synonyms.append(word_compound)
                synonyms.append(synonym.translate(None, Wordnet.__separator__))
            else:
                synonyms.append(synonym)
        return [w.lower() for w in synonyms]

    def preprocess_query(self, words):
        synonyms = [syn for term in words
                    for syn in Wordnet.synonyms(term, ' '.join(words),
                                                self.documents.rawtext)]

        return words + synonyms


class Soundex(SearchEngine):
    __transtable__ = string.maketrans(
        'bfpv' + 'cgjkqsxz' + 'dt' + 'l' + 'mn' + 'r' + 'aeiouyhw',
        '1111' + '22222222' + '33' + '4' + '55' + '6' + '--------')

    @staticmethod
    def fingerprint(word):
        raw_soundex = word[0] + word[1:].translate(Soundex.__transtable__)
        soundex = ''.join(c for c, _ in itertools.groupby(raw_soundex)
                          if not c == '-')
        return (soundex + '000')[:4]

    def _get_document_words(self):
        if not hasattr(self, '_document_words'):
            self._document_words = Vector(w for _, d in self.documents
                                          for w in d.domain())
        return self._document_words

    def _get_lookup_dict(self):
        if not hasattr(self, '_lookup_dict'):
            soundex = [Soundex.fingerprint(w) for w in self._document_words]

            d = dict.fromkeys(soundex)
            for w, s in itertools.izip(self._document_words, soundex):
                if not d.get(s):
                    d[s] = [w]
                else:
                    d[s].append(w)
            for k, v in d.iteritems():
                v.sort(
                    key=lambda w: self._document_words[w],
                    reverse=True)
                d[k] = v[0] if v else None

            self._lookup_dict = d
        return self._lookup_dict

    def correct(self, word):
        if word in self._get_document_words():
            return word
        return self._get_lookup_dict().get(Soundex.fingerprint(word)) or word

    def postprocess_query(self, query):
        return query.transform(self.correct)


class Cosine(SearchEngine):
    def similarity(self, query, document):
        dot_pr = 0
        qws_sq = 0
        dws_sq = 0

        # resolve references now to avoid doing it in every loop iteration
        tf = TfIdf.term_frequency
        idf = TfIdf.inverse_document_frequency
        queries = self.queries
        documents = self.documents

        for w in query:
            tf_wq = tf(w, query, queries)
            tf_wd = tf(w, document, documents)
            idf_w = idf(w, documents)

            qw = tf_wq * idf_w
            dw = tf_wd * idf_w

            dot_pr += qw * dw
            qws_sq += qw ** 2
            dws_sq += dw ** 2

        return dot_pr / (math.sqrt(qws_sq) + math.sqrt(dws_sq))


class PseudoFeedback(TfIdf, SearchEngine):
    __num_expansions__ = 10

    def postprocess_query(self, query):
        scores = sorted([(TfIdf.similarity(self, query, document), did)
                         for did, document in self.documents], reverse=True)

        for _, did in scores[:PseudoFeedback.__num_expansions__]:
            query.union(self.documents[did])
        return query


if __name__ == '__main__':
    class BestSearchEngine(PseudoFeedback, Stemmer, Stopwords, Cosine):
        def similarity(self, query, document):
            return Cosine.similarity(self, query, document)

        def postprocess_query(self, query):
            return PseudoFeedback.postprocess_query(self, query)

        def process(self, words):
            words = Stopwords.process(self, words)
            words = words + Stemmer.process(self, words)
            return words

    parser = OptionParser(calling_module=__file__)
    query_db, document_db, outpath = parser.parse_args()
    search_engine = BestSearchEngine(query_db, document_db)
    search_engine.write_scores(outpath)
