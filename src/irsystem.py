from __future__ import division
import abc
import nltk.tokenize
import optparse
import os
import re
import string


class OptionParser(optparse.OptionParser):
    def __init__(self, calling_module):
        optparse.OptionParser.__init__(self)

        self._outname = os.path.splitext(calling_module)[0] + '.top'
        curdir = os.getcwd()

        self.add_option('-Q', '--qrys', dest='qrys_path', metavar='QRYS',
                        help='read queries from QRYS',
                        default=os.path.join(curdir, 'qrys.txt'))
        self.add_option('-D', '--docs', dest='docs_path', metavar='DOCS',
                        help='read documents from DOCS',
                        default=os.path.join(curdir, 'docs.txt'))
        self.add_option('-O', '--output', dest='out_path', metavar='OUT',
                        help='save outputs to OUT',
                        default=os.path.join(curdir, self._outname))

    def parse_args(self):
        options, _ = optparse.OptionParser.parse_args(self)

        if os.path.isdir(options.out_path):
            options.out_path = os.path.join(options.out_path, self._outname)

        return (options.qrys_path, options.docs_path, options.out_path)


class Tokenizer(object):
    __word_regex__ = re.compile("(\w[\w'.]*\w|\w)")
    __greedy_regex__ = re.compile("(\w[\w'.-]*\w|\w)")
    __separator_regex__ = re.compile("['.-]")

    @staticmethod
    def simple_split(document):
        return [w.lower() for w in Tokenizer.__word_regex__.findall(document)]

    @staticmethod
    def semantic_split(document):
        words = [w for sent in nltk.tokenize.sent_tokenize(document)
                 for w in nltk.tokenize.word_tokenize(sent)]
        words = [w.translate(None, string.punctuation) for w in words]
        return [w.lower() for w in words if w]

    @staticmethod
    def merge_split(document):
        result = []
        for word in Tokenizer.__greedy_regex__.findall(document):
            tokens = Tokenizer.__separator_regex__.split(word)
            result.extend(tokens)
            if len(tokens) > 1:
                result.append(word)
                result.append(Tokenizer.__separator_regex__.sub('', word))

        return [t.lower() for t in result]


class Vector(object):
    def __init__(self, iterable):
        if isinstance(iterable, dict):
            self._d = iterable
        else:
            d = dict.fromkeys(iterable, 0)
            for i in iterable:
                d[i] += 1
            self._d = d

    def __getitem__(self, key):
        return self._d.get(key, 0)

    def __contains__(self, key):
        return key in self._d

    def __iter__(self):
        return iter(self._d)

    def __len__(self):
        return len(self._d)

    def __repr__(self):
        return 'Vector(' + repr(self._d)[1:-1] + ')'

    def domain(self):
        return self._d.iterkeys()

    def transform(self, fn):
        for k, v in self._d.items():
            fnk = fn(k)
            if fnk != k:
                self._d[fnk] = v
                del self._d[k]
        return self

    def dot_product(self, other):
        return sum(v * other._d.get(k, 0) for k, v in self._d.iteritems())

    def union(self, other):
        for k, v in other._d.iteritems():
            self._d[k] = self._d.get(k, 0) + v
        return self


class Database(object):
    __data_format__ = re.compile('^\D*(\d+)\s+(.*)$')

    def __init__(self, path, key_t, value_t):
        db = {}
        total_len = 0
        num_entries = 0
        raw = []

        with open(path, 'r') as f:
            for line in f:
                line = line.strip()
                k, v = Database.__data_format__.match(line).groups()
                key, value = key_t(k), value_t(v)
                db[key] = value
                total_len += len(value)
                num_entries += 1
                raw.append(v)

        self._db = db
        self._len = num_entries
        self.avg_len = total_len / num_entries
        self.rawtext = ' '.join(raw)

    def __iter__(self):
        return self._db.iteritems()

    def __getitem__(self, dbkey):
        return self._db.get(dbkey)

    def __len__(self):
        return self._len


class SearchEngine(object):
    __metaclass__ = abc.ABCMeta
    __output_template__ = '{query_id} 0 {document_id} 0 {similarity} 0\n'

    @abc.abstractmethod
    def similarity(self, query, document):
        pass

    def tokenize(self, document):
        return Tokenizer.simple_split(document)

    def process(self, words):
        return words

    def preprocess_query(self, query_tokens):
        return self.process(query_tokens)

    def preprocess_document(self, document_tokens):
        return self.process(document_tokens)

    def postprocess_query(self, query):
        return query

    def run_queries(self):
        scores = dict.fromkeys((qid, did)
                               for qid, _ in self.queries
                               for did, _ in self.documents)

        for qid, query in self.queries:
            query = self.postprocess_query(query)
            for did, document in self.documents:
                scores[qid, did] = self.similarity(query, document)

        return scores

    def write_scores(self, outpath):
        scores = self.run_queries()
        with open(outpath, 'w') as f:
            for (qid, did), score in scores.iteritems():
                f.write(SearchEngine.__output_template__
                        .format(
                            query_id=qid,
                            document_id=did,
                            similarity=score))

    def __init__(self, query_db_path, document_db_path):
        qryfun = lambda q: Vector(self.preprocess_query(self.tokenize(q)))
        docfun = lambda d: Vector(self.preprocess_document(self.tokenize(d)))
        self.documents = Database(document_db_path, int, docfun)
        self.queries = Database(query_db_path, int, qryfun)
