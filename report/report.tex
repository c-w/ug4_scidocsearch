\documentclass[a4paper,twoside,twocolumn]{article}

\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{xspace}
\usepackage{titling}
\usepackage[margin=0.45in]{geometry}

\newcommand*{\eg}{e.g.\@\xspace}
\newcommand*{\ie}{i.e.\@\xspace}
\newcommand*{\sdagger}{\textsuperscript{\textdagger}}

\setlength{\droptitle}{-4em}
\date{\vspace{-2em}}
\pagenumbering{gobble}

% Write a report outlining the decisions you made in implementing the search
% algorithms, as well as your comments on the relative performance of the
% techniques you have tried.  Provide specific details on the methods and their
% parameters.  Include formulas, tables and graphs if necessary.  You are
% allowed a maximum of 2 pages for the report, but shorter reports are fine.
% Submit a paper copy of the report to the ITO.

\title{Text Technologies - Assessment 1}
\author{Clemens Wolff (s0942284)}

\begin{document}
\maketitle

\section{Introduction}\label{sec:introduction}

This report investigates how the use of different algorithms in the Ranking and
Text Transformation steps of the Information Retrieval (IR) pipeline as
described by \cite[ch.~2.2]{ir-book} affect the performance of search engines
individually (Section~\ref{sec:comparison}) and when combined
(Section~\ref{sec:combination}). It then synthesises these findings to propose
an effective retrieval algorithm for scientific documents
(Section~\ref{sec:conclusion}).

\section{Comparison of algorithms}\label{sec:comparison}

In order to evaluate the impact of different algorithms in different parts of on
the IR process, we built a skeleton search engine for our English corpus of 32
queries and 3204 scientific documents.  Pre-processing consistent in tokenizing
the queries and documents using the regular expression \verb/(\w[\w'.]*\w|\w)/
which splits words on surrounding white-space and punctuation.

The following subsections change one part of this skeleton search engine at a
time and report changes in performance in Mean Average Precision (MAP).  Note
that more complex stochastic procedures (\eg finding statistical synonyms) were
not investigated due to the limited size of the corpus.

\subsection{Domain adaptation}\label{subsec:domain}

Our corpus consists of scientific documents with a specific structure (title,
text, author, publication, references) that could be abused (\eg weigh query
terms in the title higher, use references of a relevant document to re-rank
documents based an adapted version of page-rank, \dots).  Unfortunately, not all
the documents in the corpus respect this structure so that we decided not to use
any domain specific information in our retrieval algorithms and keep our system
general-purpose.  Furthermore, some documents consist of no more than sentence
stubs which led us to not explore more involved natural language processing
inspired techniques (\eg phrase parsing).

\subsection{Similarity functions}\label{subsec:similarity}

We implemented the word overlap baseline, term frequency inverse document
frequency (TfIdf) and weighted cosine (Cosine) scoring functions described in
\cite[p.~6,10,11]{tts3}.  A noteworthy implementation detail is that we chose to
smooth the inverse document frequency component of the TfIdf scoring function in
order to avoid problems with query terms having zero occurrences in the corpus:
$idf_w = log({C + 1 \over df_w + 0.5})$ where $df_w$ is the document frequency
of word $w$ and $C$ is the number of documents in the corpus.  TfIdf's
meta-parameter $k$ was set to 2.  Modifying $k$ did not improve performance of
the final system.

As expected, the crude word overlap baseline (0.1595 MAP) is outperformed by
both TfIdf (0.3133 MAP) and Cosine (0.3093 MAP).  TfIdf outperformed Cosine
because the data-set contains mostly short queries and documents (query length
$\mu=20,~\sigma=2.4$ words; document length $\mu=74,~\sigma=0.6$ words).

TfIdf gave better results than Cosine in most of the remaining experiments
described in this report.  We therefore only report TfIdf scores from here
onwards, mentioning Cosine (denoted by \sdagger) only when it outperformed
TfIdf.  The interested reader can consult the full raw results data on-line at
\url{http://goo.gl/f0PwMb}.

\subsection{Tokenization}\label{subsec:tokenization}

In order to better handle compound words (\eg \emph{pre-diabetes}) and
abbreviations (\eg \emph{I.B.M}), we implemented split-fuse tokenization as
described in \cite[p.~5]{tts4}, raising performance of the system to
0.3160 MAP\@.  The increase in performance can be explained by the the
observation that compound words and abbreviations are quite frequent in the
data-set: 2.5\% of the words in the documents and 2.9\% of the words in the
queries are affected by the split-fuse approach (5890 and 18 words respectively
for 1859 and 15 unique tokens).

\subsection{Stop-wording}\label{subsec:stopwording}

Removing selected words during index creation can help to reduce the amount of
noise the retrieval algorithm has to deal with.  Using the list of English
stop-words included in the Natural Language Toolkit (NLTK) \cite{nltk-book}, we
were able to raise the performance of our system to 0.3255 MAP\@.  Additionally
removing any token that parses as a number increased the performance to 0.3274
MAP\@.  Removing short tokens also improved performance ($\le$ 2 characters:
0.3324 MAP, $\le$ 3 characters: 0.3275 MAP).  Using the top-50 most frequent
words in the corpus as stop-words as suggested by \cite[ch.~4.3.3]{ir-book} did
not improve performance (0.3259 MAP when used alone or 0.3275 MAP when also
stopping numbers and tokens shorter than 3 characters).

\subsection{Vocabulary mismatch}\label{subsec:mismatch}

A key problem in IR is vocabulary mismatch resulting from linguistic variation
between queries and documents \cite[ch.~1.2]{ir-book}: a query and document can
refer to the same concept but do so with different words.  Morphological
operations such as stemming, lemmatization or replacing words with their
constituent n-grams and syntactic operations such as spelling correction aim to
address this problem.  Table~\ref{tab:mismatch} lists the efficacy of these
schemes on our data-set.

\begin{table}[ht]
    \centering
    \begin{tabular}{l | c}
        \hline
            Linguistic operation & MAP \\
        \hline
            Lemmatization & 0.3332 \\
            \emph{Stemming} & \emph{0.3384} \\
            3-grams & 0.2809 \\
            4-grams & 0.3228 \\
            5-grams & 0.3127 \\
            Spelling correction & 0.3126 \\
        \hline
    \end{tabular}
    \caption{Linguistic operation comparison}
    \label{tab:mismatch}
\end{table}

Stemming and lemmatization were performed using the {\tt PorterStemmer} and
{\tt WordNetLemmatizer} functions provided by NLTK\@.  Spelling correction was
implemented using a Soundex-based look-up method: if $s(x)$ is the Soundex value
of word $x$ and $f(x)$ is how often $x$ occurs in all of the documents, then for
each query term $q$ for which $f(q) = 0$, replace $q$ with the word $d$ such
that ${arg\,max}_d f(d)$ subject to $s(q) = s(d)$.

Spelling correction, did not perform well since only 5.7\% of words benefited
from it.  The other operations increased performance. Since NLTK offers
high-quality stemmers and lemmatizers for English, both outperformed the ad-hoc
n-gram approach.

The results for stemming, lemmatization and n-grams in Table~\ref{tab:mismatch}
were obtained by replacing each word in the index with its transformed version.
It is interesting to note that merging the original index with its stemmed,
lemmatized or n-gramed version decreases performance (0.2858, 0.2708 and 0.3204
MAP respectively).

\subsection{Query expansion}\label{subsec:expansion}

Another way to deal with the vocabulary mismatch problem introduced in
Section~\ref{subsec:mismatch} is to expand queries with terms deemed relevant to
the query.  Table~\ref{tab:expansion} shows the results of two such methods:
synonym expansion \cite[p.~24-25]{tts4} and pseudo-relevance-feedback (PRF) \ie
expansion with the $n_w$ most relevant words in each of the $n_d$ most relevant
documents for the query.

Synonym expansion was implemented via retrieving a list of word-senses for each
query term from NLTK's WordNet interface, choosing the most likely sense in the
context of the query using the Lesk algorithm \cite{lesk} and augmenting the
query with all synonyms of that most likely sense.  PRF was implemented as
described in \cite[p.~39]{tts4}.

\begin{table}[ht]
    \centering
    \begin{tabular}{l | c}
        \hline
            Expansion method & MAP \\
        \hline
            Synonym expansion & 0.2696 \\
            PRF $n_d=5,n_w=\infty$ & 0.4610 \\
            \emph{PRF $n_d=6,n_w=\infty$} & \emph{0.4623} \\
            PRF $n_d=7,n_w=\infty$ & 0.4420 \\
            PRF\sdagger $n_d=6,n_w=\infty$ & 0.4518 \\
            \emph{PRF\sdagger $n_d=7,n_w=\infty$} & \emph{0.4649} \\
            PRF\sdagger $n_d=8,n_w=\infty$ & 0.4543 \\
        \hline
    \end{tabular}
    \caption{Query expansion method comparison}
    \label{tab:expansion}
\end{table}

In keeping with the literature \cite[p.~204]{ir-book}, synonym expansion with a
general purpose dictionary was detrimental to the system's performance.  Most
queries are short which means that the Lesk algorithm often lacks context and
thus fails to disambiguate the correct word-sense.  Adopting a fallback strategy
that chooses the most common sense for this particular corpus rather than the
most common sense as defined by WordNet does not affect overall performance: it
only changes the disambiguation result for 7 tokens.  PRF is highly effective
for this data-set.  Observing that removing numbers from the index reduces the
performance of the PRF system by 40\% leads to an explanatory hypothesis: for
each relevant document, PRF extends the query with the words in that document,
including its reference-numbers, which then lead to the referred documents being
retrieved in the second similarity calculation pass.  Naturally, these
referenced documents are highly relevant to the query.  This explanation also
motivates the choice of $n_w=\infty$: putting a cap on $n_w$, say, using the top
20 words ranked by inverse document score, might lead to the reference-numbers
not being included.  Note that PRF increases query sizes substantially and thus
gives the Cosine similarity metric an edge over TfIdf.  Combining both
similarity functions by using TfIdf to find the query expansion set for PRF and
Cosine to compute the actual query-document similarities improved performance
further (0.5029 MAP with $n_d=7$).

\section{Combining algorithms}\label{sec:combination}

Experimentation showed that combining components which individually increased
performance gives an additional performance boost.  For instance, combining
TfIdf, stop-wording and split-fuse tokenization with stemming increases
performance by 7.4\%.

Combining previously underperforming components can also increase performance.
For example, the previously detrimental index-merging-stemming approach, when
used together with TfIdf, stop-wording and split-fuse tokenization, increases
performance (0.3698 MAP).  The same phenomenon can be observed when combining
stemming and PRF (index-merging improves performance by 8\%).

System performance was found to be non-monotonic: good meta-parameter values for
some algorithms could turn out to be non-optimal in other situations, requiring
a full search through the space of all possible combinations to find the best
performing combination of algorithms.

\section{Conclusion}\label{sec:conclusion}

This report investigated the effect of a variety of factors on the performance
of a search engine for scientific documents.  Section~\ref{sec:comparison}
showed that removing stop-words, stemming, split-fuse tokenization, or
pseudo-relevance-feedback are all effective pre-processing steps.
Section~\ref{sec:combination} showed that combining different algorithms
increases system performance non-monotonically.

Synthesising these insights, we propose a search engine that uses Cosine
similarity, removes stop-words, expands the index with a stemmed version of each
token and uses TfIdf to select the top 10 documents to expand queries.  The
system achieves 0.5413 MAP.

\begin{thebibliography}{9}

\bibitem{ir-book}
    B. Croft, D. Metzler and T. Strohman,
    \emph{Search Engines: Information Retrieval In Practice},
    Addison-Wesley Publishing Company,
    2009

\bibitem{tts3}
    V. Lavrenko,
    \emph{Text Technologies --- Vector space model},
    Lecture at the University of Edinburgh,
    23 September 2013,
    available on-line at
        \url{http://www.inf.ed.ac.uk/teaching/courses/tts/pdf/vspace-2x2.pdf}

\bibitem{tts4}
    V. Lavrenko,
    \emph{Text Technologies --- Vocabulary mismatch},
    Lecture at the University of Edinburgh,
    26 September 2013,
    available on-line at
        \url{http://www.inf.ed.ac.uk/teaching/courses/tts/pdf/text-2x2.pdf}

\bibitem{nltk-book}
    S. Bird, E. Klein and E. Loper,
    \emph{Natural Language Processing with Python},
    O'Reilly Media Inc,
    2009

\bibitem{lesk}
    M. Lesk,
    \emph{Automatic sense disambiguation using machine readable dictionaries:
          how to tell a pine cone from an ice cream cone},
    ACM,
    1986

\end{thebibliography}

\end{document}
